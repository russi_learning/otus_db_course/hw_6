-- Задание № 1
EXPLAIN (costs, verbose true, analyse true)
SELECT * FROM product_info.price pr
INNER JOIN product_info.products p on p.id = pr.product_id
WHERE p.title = 'Xiaomi Mi 9T';

/*
Gather  (cost=2409.01..45241.00 rows=100 width=75) (actual time=3.484..478.143 rows=100 loops=1)
"  Output: pr.price_id, pr.value, pr.product_id, pr.provider_id, p.id, p.title, p.description, p.category_id, p.producer_id"
  Workers Planned: 2
  Workers Launched: 2
  ->  Hash Join  (cost=1409.01..44231.00 rows=42 width=75) (actual time=30.890..443.169 rows=33 loops=3)
"        Output: pr.price_id, pr.value, pr.product_id, pr.provider_id, p.id, p.title, p.description, p.category_id, p.producer_id"
        Inner Unique: true
        Hash Cond: (pr.product_id = p.id)
        Worker 0:  actual time=60.655..431.533 rows=23 loops=1
        Worker 1:  actual time=28.743..431.593 rows=16 loops=1
        ->  Parallel Seq Scan on product_info.price pr  (cost=0.00..38430.33 rows=1672833 width=16) (actual time=0.020..318.863 rows=1332507 loops=3)
"              Output: pr.price_id, pr.value, pr.product_id, pr.provider_id"
              Worker 0:  actual time=0.021..312.078 rows=1314972 loops=1
              Worker 1:  actual time=0.022..317.035 rows=1246744 loops=1
        ->  Hash  (cost=1409.00..1409.00 rows=1 width=59) (actual time=2.977..2.978 rows=1 loops=3)
"              Output: p.id, p.title, p.description, p.category_id, p.producer_id"
              Buckets: 1024  Batches: 1  Memory Usage: 9kB
              Worker 0:  actual time=2.848..2.848 rows=1 loops=1
              Worker 1:  actual time=2.848..2.848 rows=1 loops=1
              ->  Seq Scan on product_info.products p  (cost=0.00..1409.00 rows=1 width=59) (actual time=0.580..2.970 rows=1 loops=3)
"                    Output: p.id, p.title, p.description, p.category_id, p.producer_id"
                    Filter: ((p.title)::text = 'Xiaomi Mi 9T'::text)
                    Rows Removed by Filter: 39999
                    Worker 0:  actual time=0.593..2.840 rows=1 loops=1
                    Worker 1:  actual time=0.593..2.839 rows=1 loops=1
Planning Time: 0.152 ms
Execution Time: 478.186 ms
*/

/*
Иднекс для ускорения поиска по заголовку товара. Удалось ускорить поиск выполнение запроса.
*/
CREATE INDEX ON product_info.products(title);
analyze product_info.products;

-- Задание № 2
EXPLAIN (costs, verbose true, analyse true)
SELECT * FROM product_info.price pr
INNER JOIN product_info.products p on p.id = pr.product_id
WHERE p.title = 'Xiaomi Mi 9T';

/*
Gather  (cost=1008.45..43840.43 rows=100 width=75) (actual time=0.242..162.671 rows=100 loops=1)
"  Output: pr.price_id, pr.value, pr.product_id, pr.provider_id, p.id, p.title, p.description, p.category_id, p.producer_id"
  Workers Planned: 2
  Workers Launched: 2
  ->  Hash Join  (cost=8.44..42830.43 rows=42 width=75) (actual time=7.282..141.306 rows=33 loops=3)
"        Output: pr.price_id, pr.value, pr.product_id, pr.provider_id, p.id, p.title, p.description, p.category_id, p.producer_id"
        Inner Unique: true
        Hash Cond: (pr.product_id = p.id)
        Worker 0:  actual time=12.799..132.390 rows=21 loops=1
        Worker 1:  actual time=8.991..132.450 rows=10 loops=1
        ->  Parallel Seq Scan on product_info.price pr  (cost=0.00..38430.33 rows=1672833 width=16) (actual time=0.018..69.303 rows=1332507 loops=3)
"              Output: pr.price_id, pr.value, pr.product_id, pr.provider_id"
              Worker 0:  actual time=0.020..64.881 rows=1245319 loops=1
              Worker 1:  actual time=0.018..64.936 rows=1242067 loops=1
        ->  Hash  (cost=8.43..8.43 rows=1 width=59) (actual time=0.033..0.034 rows=1 loops=3)
"              Output: p.id, p.title, p.description, p.category_id, p.producer_id"
              Buckets: 1024  Batches: 1  Memory Usage: 9kB
              Worker 0:  actual time=0.031..0.032 rows=1 loops=1
              Worker 1:  actual time=0.033..0.034 rows=1 loops=1
              ->  Index Scan using products_title_idx on product_info.products p  (cost=0.41..8.43 rows=1 width=59) (actual time=0.028..0.029 rows=1 loops=3)
"                    Output: p.id, p.title, p.description, p.category_id, p.producer_id"
                    Index Cond: ((p.title)::text = 'Xiaomi Mi 9T'::text)
                    Worker 0:  actual time=0.025..0.026 rows=1 loops=1
                    Worker 1:  actual time=0.027..0.028 rows=1 loops=1
Planning Time: 0.506 ms
Execution Time: 162.713 ms
*/

-- Задание №3

EXPLAIN (costs, verbose true, analyse true)
SELECT title, description FROM product_info.products
WHERE to_tsvector('english', title)@@to_tsquery('english','Xiaomi');
/*
Seq Scan on product_info.products  (cost=0.00..11409.00 rows=200 width=47) (actual time=7.653..34.109 rows=50 loops=1)
"  Output: title, description"
"  Filter: (to_tsvector('english'::regconfig, (products.title)::text) @@ '''xiaomi'''::tsquery)"
  Rows Removed by Filter: 39950
Planning Time: 0.092 ms
Execution Time: 34.122 ms
*/

/*
Индекс для полнотекстового поиска. Использовал GIN, так как GIN хорошо работает в полнотекстовом поиске.
Планирование запроса заняло побольше времени чем при последовательном сканировании, но запрос выполнился
примерно в 640 раз быстрее.
*/
CREATE INDEX search_index_title ON product_info.products USING GIN (to_tsvector('english', title));
analyze product_info.products;

EXPLAIN (costs, verbose true, analyse true)
SELECT title, description FROM product_info.products
WHERE to_tsvector('english', title)@@to_tsquery('english','Xiaomi');

/*
Bitmap Heap Scan on product_info.products  (cost=12.32..160.96 rows=41 width=47) (actual time=0.019..0.025 rows=50 loops=1)
"  Output: title, description"
"  Recheck Cond: (to_tsvector('english'::regconfig, (products.title)::text) @@ '''xiaomi'''::tsquery)"
  Heap Blocks: exact=2
  ->  Bitmap Index Scan on search_index_title  (cost=0.00..12.31 rows=41 width=0) (actual time=0.013..0.013 rows=50 loops=1)
"        Index Cond: (to_tsvector('english'::regconfig, (products.title)::text) @@ '''xiaomi'''::tsquery)"
Planning Time: 0.562 ms
Execution Time: 0.049 ms
*/

-- Задание №4

EXPLAIN (costs, verbose true, analyse true)
SELECT * FROM customer_info.purchase
WHERE purchase_status = 'Cancelled';

/*
Seq Scan on customer_info.purchase  (cost=0.00..59.49 rows=152 width=22) (actual time=0.036..0.274 rows=152 loops=1)
"  Output: id, customer_id, price_id, purchase_status"
  Filter: ((purchase.purchase_status)::text = 'Cancelled'::text)
  Rows Removed by Filter: 2367
Planning Time: 0.102 ms
Execution Time: 0.293 ms
*/

-- Индекс для поиска только по отмененным заказам. Запрос ускорился в 5 раз.
CREATE INDEX status_canc ON customer_info.purchase(purchase_status) WHERE purchase_status = 'Cancelled';
analyse customer_info.purchase;

EXPLAIN (costs, verbose true, analyse true)
SELECT * FROM customer_info.purchase
WHERE purchase_status = 'Cancelled';

/*
Bitmap Heap Scan on customer_info.purchase  (cost=8.94..38.84 rows=152 width=22) (actual time=0.015..0.032 rows=152 loops=1)
"  Output: id, customer_id, price_id, purchase_status"
  Recheck Cond: ((purchase.purchase_status)::text = 'Cancelled'::text)
  Heap Blocks: exact=3
  ->  Bitmap Index Scan on status_canc  (cost=0.00..8.90 rows=152 width=0) (actual time=0.009..0.009 rows=152 loops=1)
Planning Time: 0.137 ms
Execution Time: 0.049 ms
*/

-- Задание №5
EXPLAIN (costs, verbose true, analyse true)
SELECT * FROM customer_info.purchase
WHERE purchase_status = 'Cancelled'
AND customer_id = 11;

/*
Bitmap Heap Scan on customer_info.purchase  (cost=8.91..39.19 rows=2 width=22) (actual time=0.025..0.050 rows=25 loops=1)
"  Output: id, customer_id, price_id, purchase_status"
  Recheck Cond: ((purchase.purchase_status)::text = 'Cancelled'::text)
  Filter: (purchase.customer_id = 11)
  Rows Removed by Filter: 127
  Heap Blocks: exact=3
  ->  Bitmap Index Scan on status_canc  (cost=0.00..8.90 rows=152 width=0) (actual time=0.014..0.014 rows=152 loops=1)
Planning Time: 0.096 ms
Execution Time: 0.072 ms
*/

-- Составной индекс из идентификатора покупателя и статуса заказа.
CREATE INDEX status_customer_idx ON customer_info.purchase(customer_id, purchase_status);
analyse customer_info.purchase;

EXPLAIN (costs, verbose true, analyse true)
SELECT * FROM customer_info.purchase
WHERE purchase_status = 'Cancelled'
AND customer_id = 11;

/* В данном случае проблема была в том, что данных мало и планирование заняло
больше времени чем без индекса. В данной таблице слишком мало данных, чтобы от индекса
была выгода
*/

/*
Index Scan using status_customer_idx on customer_info.purchase  (cost=0.28..10.07 rows=2 width=22) (actual time=0.016..0.022 rows=25 loops=1)
"  Output: id, customer_id, price_id, purchase_status"
  Index Cond: ((purchase.customer_id = 11) AND ((purchase.purchase_status)::text = 'Cancelled'::text))
Planning Time: 0.191 ms
Execution Time: 0.038 ms
*/